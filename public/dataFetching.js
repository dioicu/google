import React from "react";
import {search} from "./searcher.mjs";

export async function getData(query, page) {
    const options = {
        page: page ? Number(page) : 0,
        safe: false,
        parse_ads: false,
        use_mobile_ua: false,
        additional_params: {
            hl: 'en'
        }
    }
    return await search(query, options)
}

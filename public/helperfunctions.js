export function googleSearchToLinks(google){
    const array = []
    for (let elem in google){
        let children = (<div className={"w-[90vw] md:w-[40vw]"}>
            {google[elem]["url"]}<br></br>
            {google[elem].title}<br></br>
            <div className={"w-[90vw] md:w-[40vw]"}> {google[elem].description}</div>
        </div>
        )
        array.push(<a href={google[elem].url} target="_blank" key={elem}>{children} <br></br></a>)
    }
    return array
}


export function Layout({children}){
    return (<div className={"mt-14"}>
        <div className={"text-sm leading-5 mx-7 "}>{children}</div>
    </div>)
}
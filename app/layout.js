import localFont from '@next/font/local';

const myFont = localFont({src: './SF-Mono-Regular.otf'});

export default function RootLayout({children}) {
    return (
        <html className={myFont.className}>
        <head/>
        <body>{children}</body>
        </html>
    )
}

import "../globals.css"
import React from "react";
import {getData} from "../../public/dataFetching";
import {googleSearchToLinks, Layout} from "../../public/helperfunctions";

export default async function Page(props){
    let page = 0
    console.log(props.searchParams.page)
    if (props.searchParams.page) page = Number(props.searchParams.page)
    const array = googleSearchToLinks((await getData(props.params.query, page)))
    return (<><Layout>{array}</Layout>
    <div className={"mx-auto  fixed bottom-2.5 left-1/2 -translate-x-1/2"}>
        {(page === 0)?(<></>): <a className={"mr-4"} href={"https://undefxx.com/" + props.params.query + "?page="+(page-1)}>{page-1}</a>)}<a className={"ml-4"} href={"https://undefxx.com/" + props.params.query + "?page="+(page+1)}>{page+1}</a>
    </div></>)
}
[
    {
        position: 1,
        title: 'Coffee - Wikipedia',
        link: 'https://en.wikipedia.org/wiki/Coffee',
        displayed_link: 'https://en.wikipedia.org › wiki › Coffee',
        snippet: 'Coffee is a drink prepared from roasted coffee beans. Darkly colored, bitter, and slightly acidic, coffee has a stimulating effect on humans, primarily due ...',
        snippet_highlighted_words: [ 'Coffee', 'coffee', 'coffee' ],
        sitelinks: { inline: [Array] },
        rich_snippet: { bottom: [Object] },
        about_this_result: { source: [Object] },
        about_page_link: 'https://www.google.com/search?q=About+https://en.wikipedia.org/wiki/Coffee&tbm=ilp&ilps=ADNMCi0tVhSB-fGHOJYgrIxB0xlXYrPGPA',
        about_page_serpapi_link: 'https://serpapi.com/search.json?engine=google_about_this_result&google_domain=google.com&ilps=ADNMCi0tVhSB-fGHOJYgrIxB0xlXYrPGPA&q=About+https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FCoffee',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:U6oJMnF-eeUJ:https://en.wikipedia.org/wiki/Coffee&cd=13&hl=en&ct=clnk&gl=us',
        related_pages_link: 'https://www.google.com/search?q=related:https://en.wikipedia.org/wiki/Coffee+Coffee'
    },
    {
        position: 2,
        title: 'What is Coffee? - National Coffee Association',
        link: 'https://www.ncausa.org/About-Coffee/What-is-Coffee',
        displayed_link: 'https://www.ncausa.org › About Coffee',
        snippet: 'cof·fee /ˈkôfē,ˈkäfē/ noun The berries harvested from species of Coffea plants. Everyone recognizes a roasted coffee bean, but you might not recognize an actual ...',
        snippet_highlighted_words: [ 'coffee' ],
        about_this_result: { source: [Object] },
        about_page_link: 'https://www.google.com/search?q=About+https://www.ncausa.org/About-Coffee/What-is-Coffee&tbm=ilp&ilps=ADNMCi2WMeB2yuv_hvFs-AJzORijEgkEeg',
        about_page_serpapi_link: 'https://serpapi.com/search.json?engine=google_about_this_result&google_domain=google.com&ilps=ADNMCi2WMeB2yuv_hvFs-AJzORijEgkEeg&q=About+https%3A%2F%2Fwww.ncausa.org%2FAbout-Coffee%2FWhat-is-Coffee',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:ENqpL6s3VPIJ:https://www.ncausa.org/About-Coffee/What-is-Coffee&cd=14&hl=en&ct=clnk&gl=us'
    },
    {
        position: 3,
        title: 'Coffee | Origin, Types, Uses, History, & Facts | Britannica',
        link: 'https://www.britannica.com/topic/coffee',
        displayed_link: 'https://www.britannica.com › ... › Food',
        snippet: 'coffee, beverage brewed from the roasted and ground seeds of the tropical evergreen coffee plants of African origin. Coffee is one of the three most popular ...',
        snippet_highlighted_words: [ 'coffee', 'coffee', 'Coffee' ],
        about_this_result: { source: [Object] },
        about_page_link: 'https://www.google.com/search?q=About+https://www.britannica.com/topic/coffee&tbm=ilp&ilps=ADNMCi0xG2ABk5g9BrBwiawxBsBHMAwr8A',
        about_page_serpapi_link: 'https://serpapi.com/search.json?engine=google_about_this_result&google_domain=google.com&ilps=ADNMCi0xG2ABk5g9BrBwiawxBsBHMAwr8A&q=About+https%3A%2F%2Fwww.britannica.com%2Ftopic%2Fcoffee',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:Wikbu4ipU28J:https://www.britannica.com/topic/coffee&cd=15&hl=en&ct=clnk&gl=us',
        related_pages_link: 'https://www.google.com/search?q=related:https://www.britannica.com/topic/coffee+Coffee'
    },
    {
        position: 4,
        title: 'The Coffee Bean & Tea Leaf | CBTL',
        link: 'https://www.coffeebean.com/',
        displayed_link: 'https://www.coffeebean.com',
        snippet: 'Born and brewed in Southern California since 1963, The Coffee Bean & Tea Leaf® is passionate about connecting loyal customers with carefully handcrafted ...',
        snippet_highlighted_words: [ 'Coffee' ],
        about_this_result: { source: [Object] },
        about_page_link: 'https://www.google.com/search?q=About+https://www.coffeebean.com/&tbm=ilp&ilps=ADNMCi2oSYB5WqnhmnflS86OdMdpjMzz9g',
        about_page_serpapi_link: 'https://serpapi.com/search.json?engine=google_about_this_result&google_domain=google.com&ilps=ADNMCi2oSYB5WqnhmnflS86OdMdpjMzz9g&q=About+https%3A%2F%2Fwww.coffeebean.com%2F',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:WpQxSYo2c6AJ:https://www.coffeebean.com/&cd=16&hl=en&ct=clnk&gl=us',
        related_pages_link: 'https://www.google.com/search?q=related:https://www.coffeebean.com/+Coffee'
    },
    {
        position: 5,
        title: 'Coffee | The Nutrition Source',
        link: 'https://www.hsph.harvard.edu/nutritionsource/food-features/coffee/',
        displayed_link: 'https://www.hsph.harvard.edu › ... › Food Features',
        snippet: 'Coffee lovers around the world who reach for their favorite morning brew probably ... One 8-ounce cup of brewed coffee contains about 95 mg of caffeine.',
        snippet_highlighted_words: [ 'Coffee', 'coffee' ],
        sitelinks: { inline: [Array] },
        about_this_result: { source: [Object] },
        about_page_link: 'https://www.google.com/search?q=About+https://www.hsph.harvard.edu/nutritionsource/food-features/coffee/&tbm=ilp&ilps=ADNMCi3F4RO_DIqjcm9VUCXmfmpqrX5h3w',
        about_page_serpapi_link: 'https://serpapi.com/search.json?engine=google_about_this_result&google_domain=google.com&ilps=ADNMCi3F4RO_DIqjcm9VUCXmfmpqrX5h3w&q=About+https%3A%2F%2Fwww.hsph.harvard.edu%2Fnutritionsource%2Ffood-features%2Fcoffee%2F',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:aCQFR0EWgPwJ:https://www.hsph.harvard.edu/nutritionsource/food-features/coffee/&cd=17&hl=en&ct=clnk&gl=us'
    },
    {
        position: 6,
        title: '31 Coffee Brands, Ranked From Worst To Best - Tasting Table',
        link: 'https://www.tastingtable.com/718678/coffee-brands-ranked-from-worst-to-best/',
        displayed_link: 'https://www.tastingtable.com › coffee-brands-ranked-fr...',
        date: 'Feb 18, 2022',
        snippet: "From café chains to retail roasters, we've ranked some of the most popular coffee brands from worst to first.",
        snippet_highlighted_words: [ 'coffee' ],
        sitelinks: { inline: [Array] },
        about_this_result: { source: [Object] },
        about_page_link: 'https://www.google.com/search?q=About+https://www.tastingtable.com/718678/coffee-brands-ranked-from-worst-to-best/&tbm=ilp&ilps=ADNMCi1QLNcX-_O8XPpiuc7jJ0b9tMa7Gg',
        about_page_serpapi_link: 'https://serpapi.com/search.json?engine=google_about_this_result&google_domain=google.com&ilps=ADNMCi1QLNcX-_O8XPpiuc7jJ0b9tMa7Gg&q=About+https%3A%2F%2Fwww.tastingtable.com%2F718678%2Fcoffee-brands-ranked-from-worst-to-best%2F',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:O0p2m8H_t7EJ:https://www.tastingtable.com/718678/coffee-brands-ranked-from-worst-to-best/&cd=18&hl=en&ct=clnk&gl=us'
    },
    {
        position: 7,
        title: '9 Health Benefits of Coffee, Based on Science - Healthline',
        link: 'https://www.healthline.com/nutrition/top-evidence-based-health-benefits-of-coffee',
        displayed_link: 'https://www.healthline.com › Wellness Topics › Nutrition',
        snippet: 'Coffee is a popular beverage that researchers have studied extensively for its many health benefits, including its ability to increase energy levels, promote ...',
        snippet_highlighted_words: [ 'Coffee' ],
        about_this_result: { source: [Object] },
        about_page_link: 'https://www.google.com/search?q=About+https://www.healthline.com/nutrition/top-evidence-based-health-benefits-of-coffee&tbm=ilp&ilps=ADNMCi005zP34LoVlgtSYcT3k6ep4HgZPQ',
        about_page_serpapi_link: 'https://serpapi.com/search.json?engine=google_about_this_result&google_domain=google.com&ilps=ADNMCi005zP34LoVlgtSYcT3k6ep4HgZPQ&q=About+https%3A%2F%2Fwww.healthline.com%2Fnutrition%2Ftop-evidence-based-health-benefits-of-coffee',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:r1UW6FGz3F4J:https://www.healthline.com/nutrition/top-evidence-based-health-benefits-of-coffee&cd=19&hl=en&ct=clnk&gl=us'
    },
    {
        position: 8,
        title: 'Starbucks Coffee Company',
        link: 'https://www.starbucks.com/',
        displayed_link: 'https://www.starbucks.com',
        snippet: 'More than just great coffee. Explore the menu, sign up for Starbucks® Rewards, manage your gift card and more.',
        snippet_highlighted_words: [ 'coffee' ],
        about_this_result: { source: [Object] },
        about_page_link: 'https://www.google.com/search?q=About+https://www.starbucks.com/&tbm=ilp&ilps=ADNMCi0cMyV0H7KdBl4d_vac7u0R1ouGYg',
        about_page_serpapi_link: 'https://serpapi.com/search.json?engine=google_about_this_result&google_domain=google.com&ilps=ADNMCi0cMyV0H7KdBl4d_vac7u0R1ouGYg&q=About+https%3A%2F%2Fwww.starbucks.com%2F',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:1vGXgo_FlHkJ:https://www.starbucks.com/&cd=20&hl=en&ct=clnk&gl=us',
        related_pages_link: 'https://www.google.com/search?q=related:https://www.starbucks.com/+Coffee'
    },
    {
        position: 9,
        title: "Peet's Coffee: The Original Craft Coffee",
        link: 'https://www.peets.com/',
        displayed_link: 'https://www.peets.com',
        snippet: "Since 1966, Peet's Coffee has offered superior coffees and teas by sourcing the best quality coffee beans and tea leaves in the world and adhering to strict ...",
        snippet_highlighted_words: [ 'Coffee', 'coffees', 'coffee' ],
        about_this_result: { source: [Object] },
        about_page_link: 'https://www.google.com/search?q=About+https://www.peets.com/&tbm=ilp&ilps=ADNMCi2xqgiMSzEyTwg-QewuVQYGctzClw',
        about_page_serpapi_link: 'https://serpapi.com/search.json?engine=google_about_this_result&google_domain=google.com&ilps=ADNMCi2xqgiMSzEyTwg-QewuVQYGctzClw&q=About+https%3A%2F%2Fwww.peets.com%2F',
        cached_page_link: 'https://webcache.googleusercontent.com/search?q=cache:BCjzno6zP6wJ:https://www.peets.com/&cd=21&hl=en&ct=clnk&gl=us',
        related_pages_link: 'https://www.google.com/search?q=related:https://www.peets.com/+Coffee'
    }
]